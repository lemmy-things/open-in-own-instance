# Open In Own Instance

This script will allow you to open a post or comment on Lemmy in your own instance.

## Installation

Open https://gitlab.com/lemmy-things/open-in-own-instance/-/raw/main/master.user.js - your UserScript manager should take care of the rest. In case it does not, you can also create a new userscript and paste the contents of the file specified above into it.

Tested on ViolentMonkey, should probably work on other managers as well.

// ==UserScript==
// @name        Comment on own instance
// @namespace   https://gitlab.com/lemmy-things/open-in-own-instance
// @include     https://*
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_registerMenuCommand
// @version     1.0.2
// @author      tristar@lemmyfly.org
// @updateURL   https://gitlab.com/lemmy-things/open-in-own-instance/-/raw/main/master.user.js
// @copyright   Expat ("MIT") License
// @supportURL  https://matrix.to/#/@tristar:mozilla.org
// @website     https://gitlab.com/lemmy-things/open-in-own-instance
// ==/UserScript==

/*
 * Hi, welcome to t h e  c o d e.
 * Experiencing issues? Please DM me on Lemmy: tristar@lemmyfly.org or on Matrix: https://matrix.to/#/@tristar:mozilla.org

 * Copyright 2023 Flap
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

/*
 * _______ _     _ _______ __   _  ______ _______         _____   ______
 * |       |_____| |_____| | \  | |  ____ |______ |      |     | |  ____
 * |_____  |     | |     | |  \_| |_____| |______ |_____ |_____| |_____|
 *
 * 1.0.2
 *  - Fixed bug in which button on distinguished comments would redirect to a wrong link
 *  - Added this changelog
 * 1.0.1
 *  - Testing updates
 * 1.0.0
 *  - Initial release
 */

async function changeHomeserver() {
  const newHomeserver = prompt('Enter your home instance domain. No `https://` or trailing `/` please!');
  await GM_setValue('homeserver', newHomeserver);
}

GM_registerMenuCommand('Change home instance', changeHomeserver);


function addToPost(own_instance) {
  if (document.getElementById('comment-on-own-instance-script')) return;
  // add 'comment on own instance'
  const noLoginAlert = document.querySelector('.comment-form .alert .d-inline');
  if (!noLoginAlert) return;


  let linkToOriginal;
  try {
    linkToOriginal = document.querySelector('a.btn.py-0[title="link"]').getAttribute('href');
  } catch (e) {
    if (!e) return;
    linkToOriginal = window.location;
  }

  const commentOnOwn = document.createElement('span');
  commentOnOwn.innerHTML = `<a id="comment-on-own-instance-script" style="padding-left: 0.5em; font-family: monospace; color: royalblue;" href="https://${own_instance}/search?q=${encodeURIComponent(linkToOriginal)}&type=All&listingType=All&page=1&sort=TopAll">[ comment on own instance instead ]</a>`
  noLoginAlert.append(commentOnOwn)
}

function addToComments(own_instance) {
  if (!window.location.href.match(/\/post\/\d+/)) return;
  if (!document.getElementsByClassName('comments')) return;
  const commentList = document.querySelectorAll('.comment div.d-flex.flex-wrap.align-items-center.text-muted.small');
  if (!commentList) return console.log('still didnt load');
  const lastElement = commentList[commentList.length-1];

  commentList.forEach((element) => {
    if (element.getElementsByClassName('comment-reply-on-own-instance').length > 0) return;
    const openInOwnButton = document.createElement('span');
    const linkToOriginal = element.querySelectorAll('a[title="link"].btn').item(1).getAttribute('href');
    openInOwnButton.innerHTML = `<a class="comment-reply-on-own-instance" style="padding-left: 0.5em; padding-right: 0.5em; font-family: monospace; color: royalblue;" href="https://${own_instance}/search?q=${encodeURIComponent(linkToOriginal)}&type=All&listingType=All&page=1&sort=TopAll">[ C ]</a>`
    element.insertBefore(openInOwnButton, element.childNodes[5])
  })
}


(async function() {

  let own_instance = await GM_getValue('homeserver', null);
  if (!own_instance) {
    own_instance = prompt('Enter your home instance domain. No `https://` or trailing `/` please!');
    await GM_setValue('homeserver', own_instance);
  }

  // lemming detection
  if (document.head.querySelector('[name~=Description][content]').content !== 'Lemmy' || document.domain === own_instance) return;

  const container = document.querySelector('#app > .mt-4.p-0.fl-1');

  // we wanna add the buttons out right if we land on a post page immediately
  if (window.location.href.match(/\/post\/\d+/)) {
    addToPost(own_instance);
    addToComments(own_instance);

  }
  // if lemmy changes, check if we should add anything
  const pageUpdateObserver = new MutationObserver(() => {
    addToPost(own_instance);
    addToComments(own_instance);
  })

  pageUpdateObserver.observe(container, { attributes: true, childList: true, subtree: true })
})();


